<p align="center">
   <img src="https://images.gitee.com/uploads/images/2021/0523/100003_b205be64_478496.png" width="200" height="200" />
</p>

<h1 align="center"> 为梦想而创作：RXThinkCMF_AVL8_PRO权限(RBAC)及内容管理框架</h1>

## 项目介绍
EVL前后端分离开发框架【专业版版】是基于 Laravel8+AntDesign+Vue 开发的权限架构及内容管理框架，采用全新的前端UI框架，支持多主题切换，前端UI框架完全适配手机端、PAD终端以及PC电脑终端，框架内置完整的权限架构体系以及常规基础模块：用户管理、角色管理、菜单管理、职级管理、岗位管理、部门管理、系统日志、布局管理、广告管理、配置管理、字典管理、等等，旗舰版重点集成了代码生成器的功能，可以一键生成整个模块的全部代码，包括PHP后端代码以及Vue+AntDesign前端UI代码，生成后可以直接运行，无需人工开发；框架专注于为中小企业提供最佳的行业基础后台框架解决方案，执行效率、扩展性、稳定性值得信赖，操作体验流畅，使用非常便捷，欢迎大家使用及进行二次开发。

* 模块化：全新的架构和模块化的开发机制，便于灵活扩展和二次开发。
* 模型/栏目/分类信息体系：通过栏目和模型绑定，以及不同的模型类型，不同栏目可以实现差异化的功能，轻松实现诸如资讯、下载、讨论和图片等功能。通过分类信息和栏目绑定，可以自动建立索引表，轻松实现复杂的信息检索。
* 是一套基于Laravel8 + Layui开发出来的框架。
* 常用类封装，日志、缓存、验证、字典、文件（本地、七牛云）。等等，目前兼容浏览器（Chrome、Firefox、360浏览器等）
* 适用范围：可以开发OA、ERP、BPM、CRM、WMS、TMS、MIS、BI、电商平台后台、物流管理系统、快递管理系统、教务管理系统等各类管理软件。
 
## 环境要求:
* PHP >= 7.1.3
* PDO PHP Extension
* MBstring PHP Extension
* CURL PHP Extension
* 开启静态重写
* 要求环境支持pathinfo
* 要求安装Zip扩展(插件/模块市场需要)

### 功能特性
- **严谨规范：** 提供一套有利于团队协作的结构设计、编码、数据等规范。
- **高效灵活：** 清晰的分层设计、钩子行为扩展机制，解耦设计更能灵活应对需求变更。
- **严谨安全：** 清晰的系统执行流程，严谨的异常检测和安全机制，详细的日志统计，为系统保驾护航。
- **组件化：** 完善的组件化设计，丰富的表单组件，让开发列表和表单更得心应手。无需前端开发，省时省力。
- **简单上手快：** 结构清晰、代码规范、在开发快速的同时还兼顾性能的极致追求。
- **自身特色：** 权限管理、组件丰富、第三方应用多、分层解耦化设计和先进的设计思想。
- **高级进阶：** 分布式、负载均衡、集群、Redis、分库分表。 
- **命令行：** 命令行功能，一键管理应用扩展。 


## 开发者信息
* 系统名称：RXThinkCMF_AVL8_PRO前后端分离旗舰版
* 作者：三只松鼠
* 作者QQ：1175401194  
* 官网网址：[http://www.rxthink.cn/](http://www.rxthink.cn/)  
* 文档网址：[http://docs.avl.pro.rxthink.cn/](http://docs.avl.pro.rxthink.cn/)  
* 开源协议：Apache 2.0

### RXThinkCMF版本说明

| 版本名称 | 说明 | 地址 |
| :---: | :---: | :---: |
| RXThinkCMF_TP3.2专业版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP3.2 |
| RXThinkCMF_TP3.2旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP3.2_PRO |
| RXThinkCMF_TP5.1专业版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP5.1 |
| RXThinkCMF_TP5.1旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP5.1_PRO |
| RXThinkCMF_TP6.x专业版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP6 |
| RXThinkCMF_TP6.x旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_TP6_PRO |
| RXThinkCMF_LV5.8专业版 | 最新开源版本，master分支 | https://gitee.com/laravel520/RXThinkCMF_LV5.8 |
| RXThinkCMF_LV5.8旗舰版 | 最新开源版本，master分支 | https://gitee.com/laravel520/RXThinkCMF_LV5.8_PRO |
| ThinkPhp3.2+Vue+ElementUI旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_EVTP3.2_PRO |
| ThinkPhp3.2+Vue+AntDesign旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_AVTP3.2_PRO |
| ThinkPhp5.1+Vue+ElementUI旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_EVTP5.1_PRO |
| ThinkPhp5.1+Vue+AntDesign旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_AVTP5.1_PRO |
| ThinkPhp6.x+Vue+ElementUI旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_EVTP6_PRO |
| ThinkPhp6.x+Vue+AntDesign旗舰版 | 最新开源版本，master分支 | https://gitee.com/ruoxi520_admin/RXThinkCMF_AVTP6_PRO |
| Laravel8.x+Vue+ElementUI旗舰版 | 最新开源版本，master分支 | https://gitee.com/laravel520/RXThinkCMF_EVL8_PRO |
| Laravel8.x+Vue+AntDesign旗舰版 | 最新开源版本，master分支 | https://gitee.com/laravel520/RXThinkCMF_AVL8_PRO |

## 后台演示

- 演示地址：[http://manage.avl.pro.rxthink.cn/](http://manage.avl.pro.rxthink.cn/)
- 演示账号：admin
- 演示密码：123456

## 技术支持

[技术支持QQ：1175401194](http://wpa.qq.com/msgrd?v=3&amp;uin=1175401194&amp;site=qq&amp;menu=yes)

## 效果图展示

#### 效果图1
 ![效果图1](./public/uploads/demo/1.png)

#### 效果图2
 ![效果图2](./public/uploads/demo/2.png)
 
#### 效果图3
 ![效果图3](./public/uploads/demo/3.png)
 
#### 效果图4
 ![效果图4](./public/uploads/demo/4.png)
 
#### 效果图5
 ![效果图5](./public/uploads/demo/5.png)
 
#### 效果图6
 ![效果图5](./public/uploads/demo/6.png)
 
#### 效果图7
 ![效果图7](./public/uploads/demo/7.png)
 
#### 效果图8
 ![效果图8](./public/uploads/demo/8.png)
 
#### 效果图9
 ![效果图9](./public/uploads/demo/9.png)
 
#### 效果图10
 ![效果图10](./public/uploads/demo/10.png)
 
#### 效果图11
 ![效果图11](./public/uploads/demo/11.png)
 
#### 效果图12
 ![效果图12](./public/uploads/demo/12.png)
 
#### 效果图13
![效果图13](./public/uploads/demo/13.png)

#### 效果图14
![效果图14](./public/uploads/demo/14.png)

#### 效果图15
![效果图15](./public/uploads/demo/15.png)

#### 效果图16
![效果图16](./public/uploads/demo/16.png)

#### 效果图17
![效果图17](./public/uploads/demo/17.png)

#### 效果图18
![效果图18](./public/uploads/demo/18.png)

#### 效果图19
![效果图19](./public/uploads/demo/19.png)

#### 效果图20
![效果图20](./public/uploads/demo/20.png)

## 安全&缺陷
如果你发现了一个安全漏洞或缺陷，请发送邮件到 1175401194@qq.com,所有的安全漏洞都将及时得到解决。

## 鸣谢
感谢[AntDesign](https://2x.antdv.com/components/transfer-cn)、[Vue](https://2x.antdv.com/components/transfer-cn)等优秀开源项目。

## 版权信息

提供个人非商业用途免费使用，商业需授权。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2017~2020 rxthink.cn (http://www.rxthink.cn)

All rights reserved。

更多细节参阅 [LICENSE.txt](LICENSE.txt)
